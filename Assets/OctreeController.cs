﻿using UnityEngine;
using System.Collections;

public class OctreeController : MonoBehaviour
{
	private uint childDepth = 3;
	private SocketCollection sockets;
	private Transform children;
	public bool invalidate = false;

	static GameObject prefab;

	public uint ChildDepth
	{
		get
		{
			return childDepth;
		}
		set
		{
			if (childDepth > 0)
				GenerateChildren();
		}
	}

	// Use this for initialization
	void Start ()
	{
		children = transform.Find("Children");
		sockets = transform.FindChild("ChildSockets").GetComponent<SocketCollection>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (invalidate)
		{
			invalidate = false;

			prefab = gameObject;
			ChildDepth = ChildDepth;
		}
	}

	void Init(uint _childDepth, Transform parent, Transform spawnSocket)
	{
		transform.parent = parent;
		transform.position = spawnSocket.position;
		transform.localScale = prefab.transform.localScale;

		ChildDepth = _childDepth;
	}

	void GenerateChildren()
	{
		if (childDepth == 0)
		{
			for (int index = 0; index < transform.childCount; index++)
			{
				var childGO = children.GetChild(index).gameObject;
				childGO.SetActive(false);
				GameObject.Destroy(childGO);
			}
		}
		else if (children.transform.childCount > 0)
		{
			for (int index = 0; index < transform.childCount; index++)
			{
				var childGO = children.GetChild(index).gameObject;
				childGO.GetComponent<OctreeController>().ChildDepth = childDepth - 1;
			}
		}
		else
		{
			var spawnSockets = sockets.Sockets;

			for (int index = 0; index < 4; index++)
			{
				var child = GameObject.Instantiate(prefab);
				child.GetComponent<OctreeController>().Init(childDepth - 1, children, spawnSockets[index]);
			}
		}
	}
}
