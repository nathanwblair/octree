﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SocketCollection : MonoBehaviour
{
	public List<Transform> Sockets
	{
		get
		{
			var result = new List<Transform>();

			for (int index = 0; index < transform.childCount; index++)
			{
				result.Add(transform.GetChild(index));
			}

			return result;
		}
	}
}
